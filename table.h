/* table.h - MeowMeow, a stream encoder/decoder */


#ifndef _TABLE_H
#define _TABLE_H

#define ENCODER_INIT { "woof", "wooF", "woOf", "woOF", \
		       "WOof", "wOoF", "wOOf", "wOOF", \
                       "Woof", "WooF", "WoOf", "WoOF", \
		       "WOof", "WOof", "WOOf", "WOOF" }

#endif	/* _TABLE_H */
